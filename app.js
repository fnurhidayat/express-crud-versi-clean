const express = require('express')
const app = express()
const port = process.env.PORT || 3000; // => Port as long belom dipake, ini bebas.
const Book = require('./models/book')

app.use(express.json())

const handleNotFound = (req, res) => {
  res.status(404).json({
    status: false,
    message: `Book with ID ${req.params.id} not found!`,
  })
} 

app.get('/', (req, res) => {
  res.json({
    message: 'Hello World!'
  })
})

app.get('/books', (req, res) => {
  res.status(200).json({
    status: true,
    message: 'Books retrieved!',
    data: { books: Book.getAll() }
  })
})

app.get('/books/:id', (req, res) => {
  const book = Book.find(req.params.id)
  if (!book) return handleNotFound(req, res);
  res.status(200).json({
    status: true,
    message: `Book with ID ${req.params.id} retrieved!`,
    data: book 
  })
})

app.post('/books', (req, res) => {
  res.status(201).json({
    status: true,
    message: 'Books retrieved!',
    data: Book.create(req.body)
  })
})

app.put('/books/:id', (req, res) => {
  const book = Book.update(req.params.id, req.body)
  if (!book) return handleNotFound(req, res);
  res.status(200).json({
    status: true,
    message: `Book with ID ${req.params.id} updated!`,
    data: book
  })
})

app.delete('/books/:id', (req, res) => {
  const book = Book.delete(req.params.id)
  if (!book) return handleNotFound(req, res);
  res.status(204).end()
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
