const books = [
  {
    id: 1,
    title: 'Sapiens',
    author: 'Yuval Noah'
  }
]

const findIndex = id => books.findIndex(i => i.id == id);

module.exports = {
  getAll: () => books,
  find: id => books.find(i => i.id == id),
  create: ({ title, author }) => {
    /* Menggunakan Javascript optional chaining. 
     * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Optional_chaining
     * */
    const id = (books[books.length - 1]?.id || 0) + 1;
    const book = { id, title, author }
    // Memasukkan data buku baru ke dalam array. 
    books.push(book)
    return book
  },
  update: (id, { title, author }) => {
    const bookIndex = findIndex(id);
    /* Kalo index-nya kurang dari nol,
     * artinya buku tersebut tidak ada di dalam array.
     * */
    if (bookIndex < 0) return null;
    const book = books[bookIndex]
    /* Menggunakan spread operator untuk mengubah buku.
     * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax
     * */
    const updatedBook = { ...book, title, author }
    /* Menimpa buku yang sudah ada di dalam array
     * dengan data buku baru.
     * */
    books[bookIndex] = updatedBook
    /* Mengembalikan buku yang sudah diupdate
     * sebagai hasil dari fungsi ini.
     * */
    return updatedBook;
  },
  delete: id => {
    const bookIndex = findIndex(id)
    if (bookIndex < 0) return false;
    /* Menghapus buku dengan index x
     * di dalam array books.
     * */
    books.splice(bookIndex, 1)
    return true
  } 
}
